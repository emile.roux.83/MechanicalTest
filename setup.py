from setuptools import setup

setup(name='mechanicaltest',
      version='0.0',
      description="Mechanical test toolbox",
      long_description="",
      author='Emile Roux, Ludovic Charleux',
      author_email='emile.roux@univ-smb.fr',
      license='GPL v3',
      packages=['mechanicaltest'],
      zip_safe=False,
      install_requires=[
          "numpy",
          "scipy",
          "matplotlib",
          "pandas"
          ],
      )
