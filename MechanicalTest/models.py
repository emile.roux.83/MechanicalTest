import numpy as np
import pandas as pd
#from argiope import mesh as Mesh
import argiope, MechanicalTest
import os, subprocess, inspect
from string import Template

# PATH TO MODULE
import MechanicalTest
MODPATH = os.path.dirname(inspect.getfile(MechanicalTest))


################################################################################
# MODEL DEFINITION
################################################################################
class Traction2D(argiope.models.Model, argiope.utils.Container):
  """
  2D Traction class.
  """
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    MechanicalTest.models.traction_2D_input(sample_mesh = self.parts["sample"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label))
                                   
                                   
    
  def write_postproc(self):
    """
    Writes the prosproc scripts
    """
    if self.solver == "abaqus":
      MechanicalTest.postproc.Traction_abqpostproc(
          path = "{0}/{1}_abqpp.py".format(
              self.workdir,
              self.label),
          label = self.label,    
          solver= self.solver)
  
  def postproc(self):
     """
     Runs the whole post proc.
     """
     self.write_postproc()
     self.run_postproc()
     #HISTORY OUTPUTS
     hist_path = self.workdir + "/reports/Traction_2D_hist.hrpt"
     if os.path.isfile(hist_path):
       hist = argiope.abq.pypostproc.read_history_report(
            hist_path, steps = self.steps, x_name = "t") 
       hist["F"] = hist.RF
       self.data["history"] = hist
     # FIELD OUTPUTS
     files = os.listdir(self.workdir + "reports/")
     files = [f for f in files if f.endswith(".frpt")]
     files.sort()
     for path in files:
       field = argiope.abq.pypostproc.read_field_report(
                           self.workdir + "reports/" + path)
       if field.part == "I_SAMPLE":
         self.parts["sample"].mesh.fields.append(field)


class REV_2D(argiope.models.Model, argiope.utils.Container):
  """
  REV_2D class.
  """
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    MechanicalTest.models.traction_2D_REV_input(sample_mesh = self.parts["sample"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label))
                                   
                                   
    
  def write_postproc(self):
    """
    Writes the prosproc scripts
    """
    if self.solver == "abaqus":
      MechanicalTest.postproc.REV_abqpostproc(
          path = "{0}/{1}_abqpp.py".format(
              self.workdir,
              self.label),
          label = self.label,    
          solver= self.solver)
  
  def postproc(self):
     """
     Runs the whole post proc.
     """
     self.write_postproc()
     self.run_postproc()
     #HISTORY OUTPUTS
     hist_path = self.workdir + "/reports/VER_2D_hist.hrpt"
     if os.path.isfile(hist_path):
       hist = argiope.abq.pypostproc.read_history_report(
            hist_path, steps = self.steps, x_name = "t") 
       hist["F"] = hist.RF
       self.data["history"] = hist
     # FIELD OUTPUTS
     files = os.listdir(self.workdir + "reports/")
     files = [f for f in files if f.endswith(".frpt")]
     files.sort()
     for path in files:
       field = argiope.abq.pypostproc.read_field_report(
                           self.workdir + "reports/" + path)
       if field.part == "I_SAMPLE":
         self.parts["sample"].mesh.fields.append(field)

class Bending2D(argiope.models.Model, argiope.utils.Container):
  """
  2D Traction class.
  """
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    MechanicalTest.models.bending_2D_input(sample_mesh = self.parts["sample"],
                                   fixtool_mesh = self.parts["fixtool"],
                                   movetool_mesh = self.parts["movingtool"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label))
                                   
                                   
    
  def write_postproc(self):
    """
    Writes the prosproc scripts
    """
    if self.solver == "abaqus":
      MechanicalTest.postproc.Bending_abqpostproc(
          path = "{0}/{1}_abqpp.py".format(
              self.workdir,
              self.label),
          label = self.label,    
          solver= self.solver)
  
  def postproc(self):
     """
     Runs the whole post proc.
     """
     self.write_postproc()
     self.run_postproc()
     #HISTORY OUTPUTS
     hist_path = self.workdir + "/reports/Bending_hist.hrpt"
     if os.path.isfile(hist_path):
       hist = argiope.abq.pypostproc.read_history_report(
            hist_path, steps = self.steps, x_name = "t") 
       hist["F"] = hist.RF
       self.data["history"] = hist
     # FIELD OUTPUTS
     files = os.listdir(self.workdir + "reports/")
     files = [f for f in files if f.endswith(".frpt")]
     files.sort()
     for path in files:
       field = argiope.abq.pypostproc.read_field_report(
                           self.workdir + "reports/" + path)
       if field.part == "I_SAMPLE":
         self.parts["sample"].mesh.fields.append(field)
       if field.part == "I_MOVETOOL":
         self.parts["movingtool"].mesh.fields.append(field)
       if field.part == "I_FIXTOOL":
         self.parts["fixtool"].mesh.fields.append(field)



################################################################################
# MESH PROCESSING
################################################################################
"""
def process_2D_sample_mesh(part):

   #Processes a 2D mesh, indenter or sample

  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True
  mesh.element_set_to_node_set(tag = "SURFACE")
  mesh.element_set_to_node_set(tag = "BOTTOM")
  mesh.element_set_to_node_set(tag = "AXIS")
  del mesh.elements[("sets", "SURFACE", "")]
  del mesh.elements[("sets", "BOTTOM", "")]
  del mesh.elements[("sets", "AXIS", "")]
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 
  mesh.node_set_to_surface("SURFACE")
  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  return mesh                                      
"""

def process_2D_tensil_mesh(part):
  """
  Processes a raw gmsh 2D tensil mesh  (1/4 model)
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True
  
  mesh.element_set_to_node_set(tag = "SURFACE")
  mesh.element_set_to_node_set(tag = "SYM_X")
  mesh.element_set_to_node_set(tag = "SYM_Y")
  mesh.element_set_to_node_set(tag = "TOP")
  del mesh.elements.sets["SURFACE"]
  del mesh.elements.sets["SYM_X"]
  del mesh.elements.sets["SYM_Y"]
  del mesh.elements.sets["TOP"]
  
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 
  mesh.node_set_to_surface("SURFACE")
  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values
  mesh.nodes[("sets","REF_NODE")] = (x == 0) * (y == y.max())


  
  return mesh        


def process_2D_REV_mesh(part):
  """
  Processes a raw gmsh 2D REV mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True

  
#  mesh.element_set_to_node_set(tag = "SURFACE")
#  mesh.element_set_to_node_set(tag = "SYM_X")
#  mesh.element_set_to_node_set(tag = "SYM_Y")
  mesh.element_set_to_node_set(tag = "TOP")

  mesh.node_set_to_surface("TOP")
  del mesh.elements.sets["BOT"]
  del mesh.elements.sets["LEFT"]
  del mesh.elements.sets["TOP"]
  del mesh.elements.sets["RIGHT"]
  
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values
  mesh.nodes[("sets","REF_NODE_TOP_RIGHT_CORNER")] = (x == x.max()) * (y == y.max())
  mesh.nodes[("sets","TOP")]   =  ( x <  x.max()) * ( y == y.max())
  mesh.nodes[("sets","RIGHT")] =  ( x == x.max()) * ( y  < y.max())
  mesh.nodes[("sets","BOT")]   =  ( y == y.min())
  mesh.nodes[("sets","LEFT")]  =  ( x == x.min())
  
  
  return mesh 

def process_2D_Bending_mesh(part):
  """
  Processes a raw gmsh 2D Bending mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True

  
#  mesh.element_set_to_node_set(tag = "SURFACE")
#  mesh.element_set_to_node_set(tag = "SYM_X")
#  mesh.element_set_to_node_set(tag = "SYM_Y")
  mesh.element_set_to_node_set(tag = "SURFACE")

  mesh.node_set_to_surface("SURFACE")
  del mesh.elements.sets["SURFACE"]
 
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values

  mesh.nodes[("sets","TOP")]   =  ( x <  x.max()) * ( y == y.max())
  mesh.nodes[("sets","RIGHT")] =  ( x == x.max()) * ( y  < y.max())
  mesh.nodes[("sets","BOT")]   =  ( y == y.min())
  mesh.nodes[("sets","LEFT")]  =  ( x == x.min())
  mesh.nodes[("sets","DIC")] = ((x < 15.) * (y > .2) * (y < 9.8))
  
   
  return mesh 

def process_2D_BendingTool_mesh(part):
  """
  Processes a raw gmsh 2D Bending mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True

  
  mesh.element_set_to_node_set(tag = "SURFACE")

  mesh.node_set_to_surface("SURFACE")
  del mesh.elements.sets["SURFACE"]

  
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values
  mesh.nodes[("sets","REF_NODE")] = (x == x.max())

  
  
  return mesh 

################################################################################
# PARTS
################################################################################  


class Sample(argiope.models.Part):
  pass
   
class Sample2D_quarter(Sample):
  """
  A 2D tensil mesh (1/4 model)
  """
  def __init__(self, L = 20., l = 3.,
               r = 2., lc1 = 0.2, 
                     **kwargs):
    self.L = L
    self.l = l
    self.r = r
    self.lc1 = lc1
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/TensileTest2D/TensileSample.geo").read())
    geo = geo.substitute(
        L = self.L,
        l = self.l,
        r = self.r,
        lc1 = self.lc1)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_tensil_mesh(self)

class Sample2D_REV(Sample):
  """
  A 2D RVE mesh
  """
  def __init__(self, lc1 = 30, 
                     **kwargs):
    self.lc1 = lc1
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/TensileTest2D_VER/VER.geo").read())
    geo = geo.substitute(
        lc1 = self.lc1)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_REV_mesh(self)
    
class Sample2D_Bending(Sample):
  """
  A 2D Bending mesh
  """
  def __init__(self, lc1 = .1,
                     h=5.,
                     L=10,
                     Nx=50,
                     Ny=15,
                     **kwargs):
    self.lc1 = lc1
    self.h=h
    self.L=L
    self.Nx=Nx
    self.Ny=Ny
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Bending/BendingSample.geo").read())
    geo = geo.substitute(
        lc1 = self.lc1,
        h=self.h,
        L=self.L,
        Nx=self.Nx,
        Ny=self.Ny)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_Bending_mesh(self)

class Sample2D_BendingTool(Sample):
  """
  A 2D Bending tool mesh
  """
  def __init__(self, lc = .1,
                     R=5.,
                     cx=0.,
                     cy=0.,                     
                     **kwargs):
    self.lc = lc
    self.R=R
    self.cx=cx
    self.cy=cy
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Bending/Tool.geo").read())
    geo = geo.substitute(
        lc = self.lc,
        R  = self.R,
        cx = self.cx,
        cy = self.cy)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_BendingTool_mesh(self)
################################################################################
# 2D STEP
################################################################################  
class Step2D_traction:
  """
  A general purpose 2D tensil step.
  """
  def __init__(self, control_type = "disp", 
                     name = "STEP", 
                     duration = 1., 
                     nframes = 100,
                     kind = "fixed", 
                     controlled_value = .1,
                     min_frame_duration = 1.e-8,
                     field_output_frequency = 99999,
                     solver = "abaqus",
                     rootPath = "/templates/models/TensileTest2D/"):
    self.control_type = control_type
    self.name = name  
    self.duration = duration
    self.nframes = nframes
    self.kind = kind  
    self.controlled_value = controlled_value
    self.min_frame_duration = min_frame_duration
    self.field_output_frequency = field_output_frequency
    self.solver = solver
    self.rootPath =rootPath
                     
  def get_input(self):
    control_type = self.control_type 
    name = self.name 
    duration = self.duration
    nframes = self.nframes
    kind = self.kind 
    controlled_value = self.controlled_value
    min_frame_duration = self.min_frame_duration
    solver = self.solver
    rootPath = self.rootPath
    if solver == "abaqus":
      if kind == "fixed":
        if control_type == "disp":
          pattern = rootPath + "need_to_be_done.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)
      if kind == "adaptative":
        if control_type == "disp":
          pattern = rootPath + "Traction_2D_step_disp_control.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           MINFRAMEDURATION = min_frame_duration,
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)                           


class Step2D_shear:
  """
  A general purpose 2D shear step.
  """
  def __init__(self, control_type = "disp", 
                     name = "STEP", 
                     duration = 1., 
                     nframes = 100,
                     kind = "fixed", 
                     controlled_value = .1,
                     min_frame_duration = 1.e-8,
                     field_output_frequency = 99999,
                     solver = "abaqus",
                     rootPath = "/templates/models/TensileTest2D/"):
    self.control_type = control_type
    self.name = name  
    self.duration = duration
    self.nframes = nframes
    self.kind = kind  
    self.controlled_value = controlled_value
    self.min_frame_duration = min_frame_duration
    self.field_output_frequency = field_output_frequency
    self.solver = solver
    self.rootPath =rootPath
                     
  def get_input(self):
    control_type = self.control_type 
    name = self.name 
    duration = self.duration
    nframes = self.nframes
    kind = self.kind 
    controlled_value = self.controlled_value
    min_frame_duration = self.min_frame_duration
    solver = self.solver
    rootPath = self.rootPath
    if solver == "abaqus":
      if kind == "fixed":
        if control_type == "disp":
          pattern = rootPath + "need_to_be_done.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)
      if kind == "adaptative":
        if control_type == "disp":
          pattern = rootPath + "Shear_2D_step_disp_control.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           MINFRAMEDURATION = min_frame_duration,
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)                       

class Step2D_bending:
  """
  A general purpose 2D bending step.
  """
  def __init__(self, control_type = "disp", 
                     name = "STEP", 
                     duration = 1., 
                     nframes = 100,
                     kind = "fixed", 
                     controlled_value = .1,
                     min_frame_duration = 1.e-8,
                     field_output_frequency = 99999,
                     solver = "abaqus",
                     rootPath = "/templates/models/Bending/"):
    self.control_type = control_type
    self.name = name  
    self.duration = duration
    self.nframes = nframes
    self.kind = kind  
    self.controlled_value = controlled_value
    self.min_frame_duration = min_frame_duration
    self.field_output_frequency = field_output_frequency
    self.solver = solver
    self.rootPath =rootPath
                     
  def get_input(self):
    control_type = self.control_type 
    name = self.name 
    duration = self.duration
    nframes = self.nframes
    kind = self.kind 
    controlled_value = self.controlled_value
    min_frame_duration = self.min_frame_duration
    solver = self.solver
    rootPath = self.rootPath
    if solver == "abaqus":
      if kind == "fixed":
        if control_type == "disp":
          pattern = rootPath + "need_to_be_done.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)
      if kind == "adaptative":
        if control_type == "disp":
          pattern = rootPath + "Bending_2D_step_disp_control.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           MINFRAMEDURATION = min_frame_duration,              
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)  

################################################################################
# 2D ABAQUS INPUT FILE
################################################################################  
def traction_2D_input(sample_mesh,
                         steps, 
                         materials,
                         path = None, 
                         element_map = None, 
                         solver = "abaqus",
                         make_mesh = True):
  """
  Returns a tensil input file.
  """
  if make_mesh:
    sample_mesh.make_mesh()

    
  if solver == "abaqus":
    pattern = Template(
        open(MODPATH + "/templates/models/TensileTest2D/Traction_2D.inp")
        .read())
    
    pattern = pattern.substitute(
        SAMPLE_MESH = sample_mesh.mesh.write_inp(),
        STEPS = "".join([step.get_input() for step in steps]),
        MATERIALS = "\n".join([m.write_inp() for m in materials]) )
  if path == None:            
    return pattern
  else:
    open(path, "w").write(pattern)  


################################################################################
# 2D ABAQUS INPUT FILE : REV
################################################################################  
def traction_2D_REV_input(sample_mesh,
                         steps, 
                         materials,
                         path = None, 
                         element_map = None, 
                         solver = "abaqus",
                         make_mesh = True):
  """
  Returns a tensil input file.
  """
  if make_mesh:
    sample_mesh.make_mesh()

    
  if solver == "abaqus":
    pattern = Template(
        open(MODPATH + "/templates/models/TensileTest2D_VER/Traction_2D.inp")
        .read())
    
    pattern = pattern.substitute(
        SAMPLE_MESH = sample_mesh.mesh.write_inp(),
        STEPS = "".join([step.get_input() for step in steps]),
        MATERIALS = "\n".join([m.write_inp() for m in materials]) )
  if path == None:            
    return pattern
  else:
    open(path, "w").write(pattern)  
    
################################################################################
# 2D ABAQUS INPUT FILE : Bending
################################################################################  
def bending_2D_input(sample_mesh,fixtool_mesh,movetool_mesh,
                         steps, 
                         materials,
                         path = None, 
                         element_map = None, 
                         solver = "abaqus",
                         make_mesh = True):
  """
  Returns a tensil input file.
  """
  if make_mesh:
    sample_mesh.make_mesh()
    fixtool_mesh.make_mesh()
    movetool_mesh.make_mesh()

    
  if solver == "abaqus":
    pattern = Template(
        open(MODPATH + "/templates/models/Bending/Bending_2D.inp")
        .read())
    
    pattern = pattern.substitute(
        SAMPLE_MESH = sample_mesh.mesh.write_inp(),
        FIXTOOL = fixtool_mesh.mesh.write_inp(),
        MOVETOOL = movetool_mesh.mesh.write_inp(),
        STEPS = "".join([step.get_input() for step in steps]),
        MATERIALS = "\n".join([m.write_inp() for m in materials]) )
  if path == None:            
    return pattern
  else:
    open(path, "w").write(pattern)  
