import numpy as np
import pandas as pd
#from argiope import mesh as Mesh
import argiope, MechanicalTest
import os, subprocess, inspect
from string import Template

# PATH TO MODULE
import MechanicalTest 
MODPATH = os.path.dirname(inspect.getfile(MechanicalTest))

################################################################################
# MODEL DEFINITION
################################################################################
class Traction2D(argiope.models.Model, argiope.utils.Container):
  """
  2D Traction class.
  """
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    MechanicalTest.models.indentation_2D_input(sample_mesh = self.parts["sample"],
                                   indenter_mesh = self.parts["indenter"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label))
                                   
                                   
    
  def write_postproc(self):
    """
    Writes the prosproc scripts
    """
    if self.solver == "abaqus":
      hardness.postproc.indentation_abqpostproc(
          path = "{0}/{1}_abqpp.py".format(
              self.workdir,
              self.label),
          label = self.label,    
          solver= self.solver)
  
  def postproc(self):
     """
     Runs the whole post proc.
     """
     self.write_postproc()
     self.run_postproc()
     #HISTORY OUTPUTS
     hist_path = self.workdir + "/reports/indentation_2D_hist.hrpt"
     if os.path.isfile(hist_path):
       hist = argiope.abq.pypostproc.read_history_report(
            hist_path, steps = self.steps, x_name = "t") 
       hist["F"] = hist.CF + hist.RF
       self.data["history"] = hist
     # FIELD OUTPUTS
     files = os.listdir(self.workdir + "reports/")
     files = [f for f in files if f.endswith(".frpt")]
     files.sort()
     for path in files:
       field = argiope.abq.pypostproc.read_field_report(
                           self.workdir + "reports/" + path)
       if field.part == "I_SAMPLE":
         self.parts["sample"].mesh.fields.append(field)
       if field.part == "I_INDENTER":
         self.parts["indenter"].mesh.fields.append(field)




def sample_mesh_2D_VER(gmsh_path, workdir, lc1 = 10, geoPath = "sample_mesh_2D"):
  """
  Builds an tensile sample mesh.
  """
  geo = Template(
        open(MODPATH + "/templates/models/TensileTest2D_VER/VER.geo").read())
  geo = geo.substitute(
        lc1 = lc1)
  open(workdir + geoPath + ".geo", "w").write(geo)
  p = subprocess.Popen("{0} -2 -algo delquad {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
#  p = subprocess.Popen("{0} -2 {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
  trash = p.communicate()
  mesh = Mesh.read_msh(workdir + geoPath + ".msh")
#  mesh.element_set_to_node_set(tag = "BOT")
#  mesh.element_set_to_node_set(tag = "LEFT")
#  mesh.element_set_to_node_set(tag = "TOP")
#  mesh.element_set_to_node_set(tag = "RIGHT")
  del mesh.elements.sets["BOT"]
  del mesh.elements.sets["LEFT"]
  del mesh.elements.sets["TOP"]
  del mesh.elements.sets["RIGHT"]
  mesh.elements.data = mesh.elements.data[mesh.elements.data.etype != "Line2"] 
  mesh.nodes.add_set_by_func("REF_NODE_TOP_RIGHT_CORNER", lambda x, y, z, labels: ((x == x.max()) * (y == y.max())) == True ) 
  mesh.nodes.add_set_by_func("TOP", lambda x, y, z, labels: ((x < x.max()) * (y == y.max())) == True ) 
  mesh.nodes.add_set_by_func("RIGHT", lambda x, y, z, labels: ((x == x.max()) * (y < y.max())) == True )
  mesh.nodes.add_set_by_func("BOT", lambda x, y, z, labels: ( y == y.min() ) == True ) 
  mesh.nodes.add_set_by_func("LEFT", lambda x, y, z, labels: ( x == x.min() ) == True ) 
  return mesh

def sample_mesh_2D_quarter(gmsh_path, workdir, L = 20., l = 3., r = 2., lc1 = 0.2, geoPath = "sample_mesh_2D"):
  """
  Builds an tensile sample mesh.
  """
  geo = Template(
        open(MODPATH + "/templates/models/TensileTest2D/TensileSample.geo").read())
  geo = geo.substitute(
        L = L,
        l = l,
        r = r,
        lc1 = lc1)
  open(workdir + geoPath + ".geo", "w").write(geo)
  p = subprocess.Popen("{0} -2 -algo delquad {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
#  p = subprocess.Popen("{0} -2 {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
  trash = p.communicate()
  mesh = Mesh.read_msh(workdir + geoPath + ".msh")
  mesh.element_set_to_node_set(tag = "SURFACE")
  mesh.element_set_to_node_set(tag = "SYM_X")
  mesh.element_set_to_node_set(tag = "SYM_Y")
  mesh.element_set_to_node_set(tag = "TOP")
  del mesh.elements.sets["SURFACE"]
  del mesh.elements.sets["SYM_X"]
  del mesh.elements.sets["SYM_Y"]
  del mesh.elements.sets["TOP"]
  mesh.elements.data = mesh.elements.data[mesh.elements.data.etype != "Line2"] 
  mesh.node_set_to_surface("SURFACE")
  mesh.elements.add_set("ALL_ELEMENTS", mesh.elements.data.index)
  mesh.nodes.add_set_by_func("REF_NODE", lambda x, y, z, labels: ((x == 0.) * (y == y.max())) == True )    
  return mesh

def sample_mesh_2D_full(gmsh_path, workdir, L = 20., l = 3., r = 2., lc1 = 0.2, geoPath = "sample_mesh_2D"):
  """
  Builds an tensile sample mesh.
  """
  geo = Template(
        open(MODPATH + "/templates/models/TensileTest2D/TensileSample_full.geo").read())
  geo = geo.substitute(
        L = L,
        l = l,
        r = r,
        lc1 = lc1)
  open(workdir + geoPath + ".geo", "w").write(geo)
  p = subprocess.Popen("{0} -2 -algo delquad {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
#  p = subprocess.Popen("{0} -2 {1}".format(gmsh_path, geoPath + ".geo"), cwd = workdir, shell=True, stdout = subprocess.PIPE)
  trash = p.communicate()
  mesh = Mesh.read_msh(workdir + geoPath + ".msh")
  
  
  mesh.element_set_to_node_set(tag = "SURFACE")
  mesh.element_set_to_node_set(tag = "TOP")
  mesh.element_set_to_node_set(tag = "BOT")
  del mesh.elements.sets["SURFACE"]
  del mesh.elements.sets["BOT"]
  del mesh.elements.sets["TOP"]
  mesh.elements.data = mesh.elements.data[mesh.elements.data.etype != "Line2"] 
  mesh.node_set_to_surface("SURFACE")
  mesh.elements.add_set("ALL_ELEMENTS", mesh.elements.data.index)
  mesh.nodes.add_set_by_func("REF_NODE_TOP", lambda x, y, z, labels: ((x == 0.) * (y == y.max())) == True )
  mesh.nodes.add_set_by_func("REF_NODE_BOT", lambda x, y, z, labels: ((x == 0.) * (y == y.min())) == True )
  return mesh

def shear_2D_step_input(control_type = "disp", name = "STEP", duration = 1., nframes = 100, controlled_value = .1, template_dir = "TensileTest2D" ):
  if control_type == "disp":
    pattern = "/templates/models/"+template_dir+"/Shear_2D_step_disp_control.inp"
  if control_type == "force":
    pattern = "/templates/models/"+template_dir+"/indentation_2D_step_load_control.inp"  
  pattern = Template(open(MODPATH + pattern).read())
          
  return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, )
                           
def tension_2D_step_input(control_type = "disp", name = "STEP", duration = 1., nframes = 100, controlled_value = .1, template_dir = "TensileTest2D" ):
  if control_type == "disp":
    pattern = "/templates/models/"+template_dir+"/Traction_2D_step_disp_control.inp"
  if control_type == "force":
    pattern = "/templates/models/"+template_dir+"/indentation_2D_step_load_control.inp"  
  pattern = Template(open(MODPATH + pattern).read())
          
  return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, )
  
def tension_2D_input(sample_mesh,
                         steps, 
                         path = None, 
                         element_map = None,
                         frames = 100,
                         template_dir = "TensileTest2D"):
  """
  Returns a indentation INP file.
  """
  pattern = Template(
          open(MODPATH + "/templates/models/"+template_dir+"/Traction_2D.inp").read())
  
  if element_map == None:
    element_map = {"Quad4": "CPS4R",
                   "Tri3":  "CPS3", }
  pattern = pattern.substitute(
      SAMPLE_MESH = sample_mesh.to_inp(element_map = element_map),
      STEPS = "".join(steps))
  if path == None:            
    return pattern
  else:
    open(path, "wb").write(pattern)  

def deleteFiles(workdir, simName):
    """
    Deletes old job files.
    """
    suffixes = ['.odb', '.lck', '.log', '.dat', '.com', '.sim', '.sta', '.prt', '.msg']
    for s in suffixes:
      path = workdir + simName + s
      try:
        os.remove(path)
      except OSError:
        pass

def run(simName = "Simu",ABAQUS_PATH="", deleteOldFiles = True, cpus=1, workdir=""):
    '''
    Runs the simulation.
    
    :param deleteOldFiles: indicates if existing simulation files are deleted before the simulation starts.
    :type deleteOlfFiles: boolean
    '''
    if deleteOldFiles:deleteFiles(workdir, simName)
    t0 = time.time()
    print '< Running simulation {0} in Abaqus>'.format(simName) 
    p = subprocess.Popen( '{0} job={1} input={1}.inp cpus={2} interactive'.format(ABAQUS_PATH, simName, cpus), cwd = workdir, shell=True, stdout = subprocess.PIPE)
    trash = p.communicate()
    print trash[0]
    t1 = time.time()
    duration = t1 - t0
    print '< Ran {0} in Abaqus: duration {1:.2f}s>'.format(simName, t1 - t0)   


