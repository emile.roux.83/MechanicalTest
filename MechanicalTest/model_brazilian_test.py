import numpy as np
import pandas as pd
#from argiope import mesh as Mesh
import argiope, MechanicalTest
import os, subprocess, inspect
from string import Template
import time
import gmsh
# PATH TO MODULE
import MechanicalTest
MODPATH = os.path.dirname(inspect.getfile(MechanicalTest))


################################################################################
# MODEL DEFINITION
################################################################################
class BrazilianTest2D(argiope.models.Model, argiope.utils.Container):
  """
  BrazilianTest2D class.
  """
  def __init__(self, friction = .1,
                     **kwargs):
    self.friction = friction
    super().__init__(**kwargs)
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    BT_2D_input(sample_mesh = self.parts["sample"],
                                   fixtool_mesh = self.parts["fixtool"],
                                   movetool_mesh = self.parts["movingtool"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label),
                                   friction=self.friction        )
                                   
                                   
    
  def write_postproc(self):
    """
    Writes the prosproc scripts
    """
    if self.solver == "abaqus":
      MechanicalTest.postproc.BT_abqpostproc(
          path = "{0}/{1}_abqpp.py".format(
              self.workdir,
              self.label),
          label = self.label,    
          solver= self.solver)
  
  def postproc(self):
     """
     Runs the whole post proc.
     """
     
     self.write_postproc()
     self.run_postproc()
     #HISTORY OUTPUTS
     hist_path = self.workdir + "reports/" + self.label + "_hist.hrpt"
     print("hist_path=",hist_path)
     if os.path.isfile(hist_path):
       hist = argiope.abq.pypostproc.read_history_report(
            hist_path, steps = self.steps, x_name = "t") 
       hist["F"] = hist.RF
       self.data["history"] = hist
     # FIELD OUTPUTS
     files = os.listdir(self.workdir + "reports/")
     files = [f for f in files if f.endswith(".frpt")]
     files.sort()
     for path in files:
       field = argiope.abq.pypostproc.read_field_report(
                           self.workdir + "reports/" + path)
       if field.part == "I_SAMPLE":
         self.parts["sample"].mesh.fields.append(field)
       if field.part == "I_MOVETOOL":
         self.parts["movingtool"].mesh.fields.append(field)
       if field.part == "I_FIXTOOL":
         self.parts["fixtool"].mesh.fields.append(field)


class BrazilianTest3D(BrazilianTest2D):
  """
  BrazilianTest3D class.
  """
  def __init__(self,
                     **kwargs):
    super().__init__(**kwargs)
    
  def write_input(self):
    """
    Writes the input file in the chosen format.
    """
    BT_3D_input(sample_mesh = self.parts["sample"],
                                   fixtool_mesh = self.parts["fixtool"],
                                   movetool_mesh = self.parts["movingtool"],
                                   steps = self.steps,
                                   materials = self.materials,
                                   solver = self.solver,
                                   path = "{0}/{1}.inp".format(self.workdir,
                                                               self.label),
                                   friction=self.friction        )


################################################################################
# MESH PROCESSING
################################################################################

       

def process_2D_BT_mesh(part):
  """
  Processes a raw gmsh 2D BT mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True

  
  mesh.element_set_to_node_set(tag = "SURFACE")

  mesh.node_set_to_surface("SURFACE")
  del mesh.elements.sets["SURFACE"]
 
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values


  
   
  return mesh 


def process_2D_Tool_mesh(part):
  """
  Processes a raw gmsh 2D Tool mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True

  
  mesh.element_set_to_node_set(tag = "SURFACE")

  mesh.node_set_to_surface("SURFACE")
  del mesh.elements.sets["SURFACE"]

  
  mesh.elements = mesh.elements.loc[mesh.space() == 2] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values
  mesh.nodes[("sets","REF_NODE")] = (x == x.max())* (y == y.max())

  
  
  return mesh 

def process_3D_BT_mesh(part):
  """
  Processes a raw gmsh 3D BT mesh  
  """
  mesh = part.mesh
  element_map = part.element_map
  material_map = part.material_map
  #mesh = process_2D_sample_mesh(part)  
  
  mesh.elements[("sets", "ALL_ELEMENTS", "")] = True
  mesh.nodes[("sets", "ALL_NODES")] = True
  
  tags = ["SURFACE"]
  for tag in tags:
    print('Processing {}'.format(tag))
    t0 = time.time()
    mesh.element_set_to_node_set(tag = tag)
    print('\t element_set_to_node_set : {:.2f} min'.format((time.time()-t0)/60))
    t0 = time.time()
    mesh.node_set_to_surface(tag)
    print('\t node_set_to_surface : {:.2f} min'.format((time.time()-t0)/60))
    del mesh.elements.sets[tag]

  
  tags = ["SYMZ", "SYMX", "SYMY"]
  for tag in tags:
    print('Processing {}'.format(tag))
    t0 = time.time()
    mesh.element_set_to_node_set(tag = tag)
    print('\t element_set_to_node_set : {:.2f} min'.format((time.time()-t0)/60))
    del mesh.elements.sets[tag]
  """  
  mesh.element_set_to_node_set(tag = "SURFACE")
  mesh.element_set_to_node_set(tag = "SYMZ")

  mesh.node_set_to_surface("SURFACE")
  mesh.node_set_to_surface("SYMZ")
  del mesh.elements.sets["SURFACE"]
  del mesh.elements.sets["SYMZ"]
  """
  mesh.elements = mesh.elements.loc[mesh.space() == 3] 

  if element_map != None:
    mesh = element_map(mesh)
  if material_map != None:
    mesh = material_map(mesh)
  
  x, y = mesh.nodes.coords.x.values, mesh.nodes.coords.y.values


  
   
  return mesh 

################################################################################
# PARTS
################################################################################  


class Sample(argiope.models.Part):
  pass
   

class Sample2D_BT(Sample):
  """
  A 2D BT mesh
  """
  def __init__(self, lc1 = .1,
                     R=5.,
                     **kwargs):
    self.lc1 = lc1
    self.R=R
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Brazilian_test/BTSample.geo").read())
    geo = geo.substitute(
        lc = self.lc1,
        R=self.R)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_BT_mesh(self)

class Sample3D_BT(Sample):
  """
  A 3D BT mesh
  """
  def __init__(self, lc1 = .1,
                     R=5.,
                     **kwargs):
    self.lc1 = lc1
    self.R=R
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Brazilian_test/BTSample3D_Quart.geo").read())
    geo = geo.substitute(
        lc = self.lc1,
        R=self.R)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_3D_BT_mesh(self)

class Sample2D_Comp(Sample):
  """
  A 2D BT mesh
  """
  def __init__(self, L = 9.,
                     e=3.,
                     Nx=10,
                     Ny=30,
                     **kwargs):
    self.L  = L
    self.Nx = Nx
    self.Ny = Ny
    self.e  = e
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Brazilian_test/CompSample.geo").read())
    geo = geo.substitute(
        L = self.L,
        Nx=self.Nx,
        Ny=self.Ny,
        e=self.e)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_BT_mesh(self)

class Sample2D_Tool(Sample):
  """
  A 2D Bending tool mesh
  """
  def __init__(self, lc = .1,
                     hy=5.,
                     Nx=10,
                     L=10.,
                     **kwargs):
    self.lc = lc
    self.hy=hy
    self.Nx=Nx
    self.L=L
    super().__init__(**kwargs)
    
  def preprocess_mesh(self):
    geo = Template(
        open(MODPATH + "/templates/models/Brazilian_test/Tool.geo").read())
    geo = geo.substitute(
        lc  = self.lc,
        hy  = self.hy,
        Nx  = self.Nx,
        L = self.L)
    open(self.workdir + self.file_name + ".geo", "w").write(geo)

  def postprocess_mesh(self):
    self.mesh = process_2D_Tool_mesh(self)
################################################################################
# 2D STEP
################################################################################  
                        



class Step2D_BT:
  """
  A general purpose 2D BT step.
  """
  def __init__(self, control_type = "disp", 
                     name = "STEP", 
                     duration = 1., 
                     nframes = 100,
                     kind = "fixed", 
                     controlled_value = .1,
                     min_frame_duration = 1.e-8,
                     field_output_frequency = 99999,
                     solver = "abaqus",
                     rootPath = "/templates/models/Brazilian_test/"):
    self.control_type = control_type
    self.name = name  
    self.duration = duration
    self.nframes = nframes
    self.kind = kind  
    self.controlled_value = controlled_value
    self.min_frame_duration = min_frame_duration
    self.field_output_frequency = field_output_frequency
    self.solver = solver
    self.rootPath =rootPath
                     
  def get_input(self):
    control_type = self.control_type 
    name = self.name 
    duration = self.duration
    nframes = self.nframes
    kind = self.kind 
    controlled_value = self.controlled_value
    min_frame_duration = self.min_frame_duration
    solver = self.solver
    rootPath = self.rootPath
    if solver == "abaqus":
      if kind == "fixed":
        if control_type == "disp":
          pattern = rootPath + "need_to_be_done.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)
      if kind == "adaptative":
        if control_type == "disp":
          pattern = rootPath + "2D_step_disp_control.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           MINFRAMEDURATION = min_frame_duration,              
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)  

################################################################################
# 3D STEP
################################################################################  
                        



class Step3D_BT:
  """
  A general purpose 3D BT step.
  """
  def __init__(self, control_type = "disp", 
                     name = "STEP", 
                     duration = 1., 
                     nframes = 100,
                     kind = "fixed", 
                     controlled_value = .1,
                     min_frame_duration = 1.e-8,
                     field_output_frequency = 99999,
                     solver = "abaqus",
                     rootPath = "/templates/models/Brazilian_test/"):
    self.control_type = control_type
    self.name = name  
    self.duration = duration
    self.nframes = nframes
    self.kind = kind  
    self.controlled_value = controlled_value
    self.min_frame_duration = min_frame_duration
    self.field_output_frequency = field_output_frequency
    self.solver = solver
    self.rootPath =rootPath
                     
  def get_input(self):
    control_type = self.control_type 
    name = self.name 
    duration = self.duration
    nframes = self.nframes
    kind = self.kind 
    controlled_value = self.controlled_value
    min_frame_duration = self.min_frame_duration
    solver = self.solver
    rootPath = self.rootPath
    if solver == "abaqus":
      if kind == "fixed":
        if control_type == "disp":
          pattern = rootPath + "need_to_be_done.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)
      if kind == "adaptative":
        if control_type == "disp":
          pattern = rootPath + "3D_step_disp_control.inp"
        if control_type == "force":
          pattern = rootPath + "force_need_to_be_done.inp"  
        pattern = Template(open(MODPATH + pattern).read())
                
        return pattern.substitute(NAME = name,
                           CONTROLLED_VALUE = controlled_value,
                           DURATION = duration,
                           FRAMEDURATION = float(duration) / nframes, 
                           MINFRAMEDURATION = min_frame_duration,              
                           FIELD_OUTPUT_FREQUENCY = self.field_output_frequency)  

 
    
################################################################################
# 2D ABAQUS INPUT FILE : BT
################################################################################  
def BT_2D_input(sample_mesh,fixtool_mesh,movetool_mesh,
                         steps, 
                         materials,
                         path = None, 
                         element_map = None, 
                         solver = "abaqus",
                         make_mesh = True,
                         friction = 0.1):
  """
  Returns a tensil input file.
  """
  if make_mesh:
    sample_mesh.make_mesh()
    fixtool_mesh.make_mesh()
    movetool_mesh.make_mesh()

    
  if solver == "abaqus":
    pattern = Template(
        open(MODPATH + "/templates/models/Brazilian_test/BT_2D.inp")
        .read())
    
    pattern = pattern.substitute(
        SAMPLE_MESH = sample_mesh.mesh.write_inp(),
        FIXTOOL = fixtool_mesh.mesh.write_inp(),
        MOVETOOL = movetool_mesh.mesh.write_inp(),
        STEPS = "".join([step.get_input() for step in steps]),
        MATERIALS = "\n".join([m.write_inp() for m in materials]),
        FRICTION=friction)
  if path == None:            
    return pattern
  else:
    open(path, "w").write(pattern)  
    
################################################################################
# 3D ABAQUS INPUT FILE : BT
################################################################################  
def BT_3D_input(sample_mesh,fixtool_mesh,movetool_mesh,
                         steps, 
                         materials,
                         path = None, 
                         element_map = None, 
                         solver = "abaqus",
                         make_mesh = True,
                         friction = 0.1):
  """
  Returns a tensil input file.
  """
  if make_mesh:
    sample_mesh.make_mesh()
    fixtool_mesh.make_mesh()
    movetool_mesh.make_mesh()

    
  if solver == "abaqus":
    pattern = Template(
        open(MODPATH + "/templates/models/Brazilian_test/BT_3D.inp")
        .read())
    
    pattern = pattern.substitute(
        SAMPLE_MESH = sample_mesh.mesh.write_inp(),
        FIXTOOL = fixtool_mesh.mesh.write_inp(),
        MOVETOOL = movetool_mesh.mesh.write_inp(),
        STEPS = "".join([step.get_input() for step in steps]),
        MATERIALS = "\n".join([m.write_inp() for m in materials]),
        FRICTION=friction)
  if path == None:            
    return pattern
  else:
    open(path, "w").write(pattern)  
