// Gmsh project created on Tue Jul 04 13:45:43 2017
lc=0.015;

R=1.125;
cx=0;
cy=1.125;
e=2.46;

Point(1) = {cx, cy, 0, lc};
Point(2) = {cx+R, cy  , 0, 2*lc};
Point(3) = {cx  , cy+R, 0, lc};
Point(4) = {cx-R, cy, 0, 2*lc};
Point(5) = {cx , cy-R, 0, lc};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};

Line Loop(1) = {1,2,3,4};
Plane Surface(1) = {1};
Recombine Surface {1};


Extrude {0, 0, e/2} {
  Surface{1}; Layers{1/(4*lc)}; Recombine;
}

Physical Volume("ALL_ELEMENTS") = {1};
Physical Surface("SURFACE") = {17, 13, 26, 25, 21};
Physical Surface("SYMZ") = {1};
