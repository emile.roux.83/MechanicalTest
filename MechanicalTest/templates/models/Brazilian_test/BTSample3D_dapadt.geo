// Gmsh project created on Tue Jul 04 13:45:43 2017
lcglob=0.1;
lcmin=.01;
R=1.125;
cx=0;
cy=1.125;
e=2.46;

Point(1) = {cx, cy, 0, lcglob};
Point(2) = {cx+R, cy  , 0, lcglob};
Point(3) = {cx  , cy+R, 0, lcglob};
Point(4) = {cx-R, cy, 0, lcglob};
Point(5) = {cx , cy-R, 0, lcglob};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};

Line Loop(1) = {1,2,3,4};






Plane Surface(1) = {1};
Recombine Surface {1};

Field[1] = Box;
Field[1].VIn = lcmin;
Field[1].VOut = lcglob;
Field[1].XMin = -0.5;
Field[1].XMax = 0.5;
Field[1].YMax = 5;
Field[1].YMin = -5;
Field[1].ZMax = 5;
Field[1].ZMin = -5;
Background Field = 1;


Extrude {0, 0, e/2} {
  Surface{1}; Layers{1/(lcglob)}; Recombine;
}

Physical Volume("ALL_ELEMENTS") = {1};
Physical Surface("SURFACE") = {17, 13, 26, 25, 21};
Physical Surface("SYMZ") = {1};



