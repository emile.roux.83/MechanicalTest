import numpy as np
import pandas as pd
from argiope import mesh as Mesh
import argiope
import os, subprocess, inspect, time
from string import Template

# PATH TO MODULE
import MechanicalTest 
MODPATH = os.path.dirname(inspect.getfile(MechanicalTest))

def Traction_abqpostproc(path, label, solver = "abaqus"):
  """
  Writes the abqpostproc file in the workdir.
  """
  if solver == "abaqus":
    pattern = Template(
          open(MODPATH + "/templates/models/TensileTest2D_full/Traction_2D_abqpostproc.py").read())
    pattern = pattern.substitute(simName = label)
    open(path, "w").write(pattern)


def Bending_abqpostproc(path, label, solver = "abaqus"):
  """
  Writes the abqpostproc file in the workdir.
  """
  if solver == "abaqus":
    pattern = Template(
          open(MODPATH + "/templates/models/Bending/Bending_2D_abqpostproc.py").read())
    pattern = pattern.substitute(simName = label)
    open(path, "w").write(pattern)

def BT_abqpostproc(path, label, solver = "abaqus"):
  """
  Writes the abqpostproc file in the workdir.
  """
  if solver == "abaqus":
    pattern = Template(
          open(MODPATH + "/templates/models/Brazilian_test/2D_abqpostproc.py").read())
    pattern = pattern.substitute(simName = label)
    open(path, "w").write(pattern)

def REV_abqpostproc(path, label, solver = "abaqus"):
  """
  Writes the abqpostproc file in the workdir.
  """
  if solver == "abaqus":
    pattern = Template(
          open(MODPATH + "/templates/models/TensileTest2D_VER/Traction_2D_abqpostproc.py").read())
    pattern = pattern.substitute(simName = label)
    open(path, "w").write(pattern)


      
def Traction_pypostproc(workdir, simName, template_dir = "TensileTest2D"):
  """
  Writes the pypostproc file in the workdir.
  """
  pattern = Template(
        open(MODPATH + "/templates/models/TensileTest2D/Traction_2D_pypostproc.py").read()) 
  pattern = pattern.substitute(simName = simName)
  open(workdir + simName + "_pypostproc.py", "wb").write(pattern)     
  
def run_postprocs(workdir="", simName="Simu",ABAQUS_PATH=""):
   t0 = time.time()
   
   #    Run abaqus post proc
   print("Run abaqus post proc")
   p = subprocess.Popen( '{0} cae noGUI={1}_abqpostproc.py'.format(ABAQUS_PATH,simName), cwd = workdir, shell=True, stdout = subprocess.PIPE)
   trash = p.communicate()
   print(trash[0])
   
   #    Run python post proc
   print("Run python post proc : " + simName + "_pypostproc.py")
   p = subprocess.Popen( '{0} {1}_pypostproc.py'.format("python",simName), cwd = workdir, shell=True, stdout = subprocess.PIPE)
   trash = p.communicate()
   print(trash[0])
   
   t1 = time.time()
   duration = t1 - t0
   print('< Ran {0} postprocess: duration {1:.2f}s>'.format(simName, t1 - t0)) 