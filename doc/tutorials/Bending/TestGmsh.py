# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 14:35:54 2019

@author: rouxemi
"""
import subprocess
gmsh_path = 'gmsh'
gmsh_space = 2
gmsh_options = "-algo delquad"
workdir='./workdir/'
name = 'dummy1.geo'

print("{0} -{1} {2} {3}".format(gmsh_path, gmsh_space,gmsh_options, name))

p = subprocess.Popen("{0} -{1} {2} {3}".format(gmsh_path, gmsh_space,gmsh_options, name),cwd = workdir,shell=True,stdout = subprocess.PIPE)  

#subprocess.run("{0} -{1} {2} {3}".format(gmsh_path, gmsh_space,gmsh_options, name),cwd = workdir)

