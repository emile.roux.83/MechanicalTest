
# coding: utf-8

# # 4 (or 3) points bending test using Argiope & MechanicalTest
# 
# 

# In[1]:


get_ipython().magic('load_ext autoreload')
get_ipython().magic('autoreload 2')


# In[2]:


import matplotlib.pyplot as plt
import matplotlib as mpl
import MechanicalTest as mt
import argiope as ag
import pandas as pd
import numpy as np
import os, subprocess, time, local_settings, time
get_ipython().magic('matplotlib nbagg')

mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5
mpl.rcParams['contour.negative_linestyle'] = 'solid'

# USEFUL FUNCTIONS
def create_dir(path):
  try:
    os.mkdir(path)
  except:
    pass


# ## Settings

# In[14]:


# SETTINGS
workdir   = "./workdir/"
outputdir = "./outputs/"
label   = "Bending"

create_dir(workdir)
create_dir(workdir + outputdir)     


# ## Model definition

# In[25]:


#-------------------------------------------------------------------------------
# MESH DEFINITIONS
def element_map(mesh):
    mesh.elements.loc[mesh.elements.type.argiope == "tri3", ("type", "solver", "")] = "CPS3" 
    mesh.elements.loc[mesh.elements.type.argiope == "quad4", ("type", "solver", "")] = "CPS4R" 
    return mesh
    
    
def sample_material_map(mesh):
    mesh.elements["materials"] = "SAMPLE_MAT" 
    return mesh
    
def sample_Bi_material_map(mesh):
    yc=mesh.centroids_and_volumes().centroid.y
    yc=yc-yc.mean()
    mesh.elements["materials"] = "SAMPLE_MAT" 
    mesh.elements.loc[yc<0,"materials"] = "SAMPLE_MAT_2"
    return mesh

# Geometrical definition of the model in mm
L=100.
h=12.5
R=5.

# Cas favorable  : indenteur bas a l'extérieure 
indetPos = 180./2. #if 0 = 3pts bending
fixpos   = 30.

# Cas Extreme : indenteur haut a l'extérieure 
indetPos = 97./2. #if 0 = 3pts bending
fixpos   = 15.

# Cas Bend1
indetPos = 90/2. #if 0 = 3pts bending
fixpos   = 30/2

parts = {
    "sample" : mt.models.Sample2D_Bending( L = L, h = h,Nx=60., Ny=30, lc1 = 0.2, 
                                   gmsh_path = "gmsh",
                                   file_name = "dummy1", 
                                   workdir = workdir, 
                                   gmsh_space = 2, 
                                   gmsh_options = "-algo delquad",
                                   element_map = element_map,
                                   material_map = sample_Bi_material_map),
    "fixtool" : mt.models.Sample2D_BendingTool( R = R, cx=fixpos, cy=-R, lc = R/10., 
                                   gmsh_path = "gmsh",
                                   file_name = "dummy2", 
                                   workdir = workdir, 
                                   gmsh_space = 2, 
                                   gmsh_options = "-algo delquad",
                                   element_map = element_map,
                                   material_map = sample_material_map),
    "movingtool" : mt.models.Sample2D_BendingTool( R = R, cx=indetPos, cy=h+R, lc = R/10., 
                                   gmsh_path = "gmsh",
                                   file_name = "dummy3", 
                                   workdir = workdir, 
                                   gmsh_space = 2, 
                                   gmsh_options = "-algo delquad",
                                   element_map = element_map,
                                   material_map = sample_material_map)
}
                                   
materials = [ag.materials.PowerLin(label = "SAMPLE_MAT", strain_data_points = 100,
                                   young_modulus = 30e3,
                                   poisson_ratio = 0.3,
                                   hardening_exponent = 0.4,
                                   yield_stress = 150.,
                                   consistency = 370.),
             ag.materials.PowerLin(label = "SAMPLE_MAT_2", strain_data_points = 100,
                                   young_modulus = 30e3,
                                   poisson_ratio = 0.3,
                                   hardening_exponent = 0.4,
                                   yield_stress = 80.,
                                   consistency = 20.)]




#-------------------------------------------------------------------------------
# STEP DEFINTIONS

steps = [
        mt.models.Step2D_bending(name = "LOADING1",
                         control_type = "disp", 
                         duration = 1.,
                         kind = "adaptative",  
                         nframes = 100,
                         controlled_value = -10.,
                         field_output_frequency = 1),
        
        ]                                                                                                  

model0 = mt.models.Bending2D(label = label, 
                      parts = parts, 
                      steps = steps, 
                      materials = materials, 
                      solver = "abaqus", 
                      solver_path = local_settings.ABAQUS_PATH,
                      workdir = workdir,
                      verbose = True)



# In[30]:


print("1: Preprocessing ----------------------------------")
get_ipython().magic('time model0.write_input()')


# In[ ]:


print("2: Processing -------------------------------------")
get_ipython().magic('time model0.run_simulation()')
print("3: Postprocessing ---------------------------------")
get_ipython().magic('time model0.postproc()')
print("4: Saving model -----------------------------------")
get_ipython().magic('time model0.save(workdir + "model.pcklz")')


# ## Model checking 
# Mesh building and quality checking.

parts["sample"].mesh.elements.head()




i = 1
fig = plt.figure()
parts_names = parts.keys()
for name, part in parts.items(): 
    mesh = part.mesh
    patches = mesh.to_polycollection(edgecolor = "black", linewidth = .5, alpha = 1.)
    stats = mesh.stats()
    patches.set_array( stats.stats.max_abs_angular_deviation )
    patches.set_cmap(mpl.cm.YlOrRd)
    ax = fig.add_subplot(1, 1, 1)
    ax.set_aspect("equal")
    #ax.set_xlim(mesh.nodes.coords.x.min(), mesh.nodes.coords.x.max())
    #ax.set_ylim(mesh.nodes.coords.y.min(), mesh.nodes.coords.y.max())
    ax.set_xlim(-10, 110)
    ax.set_ylim(-25,30)
    ax.add_collection(patches)
cbar = plt.colorbar(patches, orientation = "horizontal")
cbar.set_label("Max Abs. Angular Deviation [$^o$]")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.grid()
plt.title(name.title())
    #i+= 1
plt.show()


model0.parts["sample"].mesh
# In[Post] :
# =============================================================================
# Reload the model for post process
# =============================================================================
model = ag.utils.load(workdir + "model.pcklz")

# ### History output
hist = model.data["history"]
hist.head()



plt.figure()
for step, group in hist.groupby("step"):
  plt.plot(-group.dtot, -group.F,'o-', label = "Step {0}".format(step))
plt.grid()
plt.legend(loc = "best")
plt.ylabel("Total force $F$, []")
plt.xlabel("Displacement, $\delta$ []")
plt.show()


# ### Fields 

meta=model.parts["sample"].mesh.fields_metadata()

parts = {k:part.mesh.copy() for k, part in model.parts.items() }

frame_num=1

# E11 strain
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

field_num = meta[(meta.label=='LE') & (meta.frame==frame_num)].index[0]
disp_num  = meta[(meta.label=='U')  & (meta.frame==frame_num)].index[0]
t  = meta[(meta.label=='U')  & (meta.frame==frame_num)].frame_value.values

levels = np.linspace(-.3, 0.0, 21)
levels = np.linspace(-.25, 0.25, 21)
dispMagnification = 1.

for k, mesh in parts.items():
    field =mesh.fields[field_num].data.v11
    disp = mesh.fields[disp_num].data
    mesh.nodes[("coords", "x")] += dispMagnification*disp.v1
    mesh.nodes[("coords", "y")] += dispMagnification*disp.v2
    tri = mesh.to_triangulation()
    patches = mesh.to_polycollection(facecolor = "none",
                                     edgecolor = "black",
                                     linewidth = .2) 
    grad = ax.tricontourf(tri, field, levels, cmap = mpl.cm.jet, alpha = 1)
    ax.tricontour(tri, field, levels, colors = "white", linewidths = 0.05)
    ax.tricontour(tri, field, 0, colors = "k", linewidths = 2)
    ax.add_collection(patches)
    if k=="sample-0":
        ax.plot(mesh.nodes[mesh.nodes.sets.DIC].coords.x,mesh.nodes[mesh.nodes.sets.DIC].coords.y,'+r')

ax.set_aspect("equal")
#ax.set_xlim(0, 55.)
#ax.set_ylim(-40, 20.)
cbar = plt.colorbar(grad)
cbar.set_label("$\epsilon_{11} (-)$")
plt.xlabel("$x$")
plt.ylabel("$y$")
#plt.grid()


# E11 strain on vertical line
sm=model.parts["sample"].mesh
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
for frame_num in range(10,106):
    field_num = meta[(meta.label=='LE') & (meta.frame==frame_num)].index[0]
    field =sm.fields[field_num].data.v11
    yn=sm.nodes.coords[sm.nodes.coords.x<1].y
    yn=yn-yn.mean()
    epsxx=field[sm.nodes.coords.x<1]
    
    plt.plot(epsxx,yn,'-')

plt.grid()

# In[ ]:
# =============================================================================
# For DIC
# =============================================================================
"""

model.parts["sample"].mesh.fields.append(model.parts["sample"].mesh.fields[7].copy())





model.parts["sample"].mesh.fields_metadata()


# In[ ]:


model.parts["sample"].mesh.fields[len(model.parts["sample"].mesh.fields)-1].label="U_DIC"


# In[ ]:


model.parts["sample"].mesh.fields_metadata()


# In[ ]:


model.parts["sample"].mesh.fields[8].data.v1+=1.0


# In[ ]:


fig = plt.figure()
ax = fig.add_subplot(1,1,1)

field_num = 4
disp_num = 7
dispDIC_num = 8
levels = np.linspace(-10, 10, 21)
dispMagnification = 0.

for k, mesh in parts.items():
    if k=="sample":
        err =mesh.fields[disp_num].data - mesh.fields[dispDIC_num].data
        disp = mesh.fields[disp_num].data
        field =mesh.fields[field_num].data.v11
        mesh.nodes[("coords", "x")] += dispMagnification*disp.v1
        mesh.nodes[("coords", "y")] += dispMagnification*disp.v2
        tri = mesh.to_triangulation()
        patches = mesh.to_polycollection(facecolor = "none",
                                         edgecolor = "black",
                                         linewidth = .2) 
        grad = ax.tricontourf(tri, err.v1, levels, cmap = mpl.cm.jet, alpha = 1)
        ax.tricontour(tri, err.v1, levels, colors = "white", linewidths = 0.05)
        ax.add_collection(patches)

        ax.plot(mesh.nodes[mesh.nodes.sets.DIC].coords.x,mesh.nodes[mesh.nodes.sets.DIC].coords.y,'+r')

ax.set_aspect("equal")
ax.set_xlim(0, 55.)
ax.set_ylim(-40, 20.)
cbar = plt.colorbar(grad)
cbar.set_label("$\epsilon_{11} (-)$")
plt.xlabel("$x$")
plt.ylabel("$y$")
#plt.grid()


# In[ ]:


err

"""
