# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 09:16:25 2016

@author: rouxemi
ref : http://opencvpython.blogspot.fr/2012/06/hi-this-article-is-tutorial-which-try.html
"""

import numpy as np
import cv2
import pygmsh as pg
 
def write_contour_geo(f,cnt,off_set_points,off_set_lines,off_set_lines_loop,off_set_surface, recombine = 0):

    # write contour
    # points
    cnt=approx.reshape(len(approx),2)
    for i in range(0, len(cnt)):
        f.write('Point({0}) = {{ {1} ,  {2} , 0., lc1 }} ;\n'.format(i+off_set_points,cnt[i,0],cnt[i,1]))
    
    
    # lines and lines loop
    s_line_loop=""
    for i in range(0, len(cnt)-1):
        f.write('Line({0}) = {{ {1} ,  {2} }} ;\n'.format(i+off_set_lines,i+off_set_points,i+off_set_points+1)) 
        s_line_loop = s_line_loop  +str(i+off_set_lines)+ ", "
    
    f.write('Line({0}) = {{ {1} ,  {2} }} ;\n'.format(len(cnt)+off_set_lines-1,len(cnt)+off_set_points-1,off_set_points)) 
    s_line_loop = s_line_loop  +str(len(cnt)+off_set_lines-1)
    f.write('Line Loop({0}) = {{ {1} }} ;\n'.format(off_set_lines_loop,s_line_loop))     
#    f.write('BSpline({0}) = {{ {1} }} ;\n'.format(off_set_lines_loop,s_line_loop)) 
    
    # surfaces
    f.write('Plane Surface( {0} ) = {{{0}}};\n'.format(off_set_surface))
    if recombine == 1 :
        f.write('Recombine Surface( {0} );\n'.format(off_set_surface))    
    
    
    off_set_points = off_set_points +  len(cnt)
    off_set_lines  = off_set_lines +  len(cnt)
    off_set_lines_loop = off_set_lines_loop + 1
    off_set_surface = off_set_surface +1
    
    return off_set_points,off_set_lines,off_set_lines_loop,off_set_surface,f 


#==============================================================================
# Imge to read 
#==============================================================================
im = cv2.imread('Test_11.jpg')
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray,156,255,0)
contours, hierarchy = cv2.findContours(thresh,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
#CV_CHAIN_APPROX_TC89_L1 CHAIN_APPROX_SIMPLE RETR_EXTERNAL  RETR_TREE


f = open('D:/Dev/GitHub/MechanicalTest/MechanicalTest/templates/models/TensileTest2D_VER/VER.geo', 'w')
f.write("lc1 = $lc1; \n")
#==============================================================================
# Recombine option 
#==============================================================================
recombine = 0


off_set_points = 1
off_set_lines = 1
off_set_lines_loop = 1
off_set_surface = 1

print(len(contours))
#cv2.drawContours(im,contours,-1,(0,255,0),3)
#cv2.waitKey(0)
for cnt in contours[0:len(contours)]:
    crit = 0.01*cv2.arcLength(cnt,True)
    crit = 1
    approx = cv2.approxPolyDP(cnt,crit,True)
    print( "approx len =", len(approx))    
    cv2.drawContours(im,[approx],0,(np.random.random_integers(0,255),np.random.random_integers(0,255),np.
    random.random_integers(0,255)),3)
    cv2.imshow('Contour', im)
    [off_set_points,off_set_lines,off_set_lines_loop,off_set_surface,f ] = write_contour_geo(f,approx,off_set_points,off_set_lines,off_set_lines_loop,off_set_surface,recombine)
    
#cv2.waitKey(0)
cv2.destroyAllWindows()

## bounding box
#x_size = 480
#y_size = 480
#off_set = 4
#f.write('Point({0}) = {{ {1} ,  {2} , 0., lc1 }} ;\n'.format(0+off_set_points,off_set,off_set))
#f.write('Point({0}) = {{ {1} ,  {2} , 0., lc1 }} ;\n'.format(1+off_set_points,x_size,off_set))
#f.write('Point({0}) = {{ {1} ,  {2} , 0., lc1 }} ;\n'.format(2+off_set_points,x_size,y_size))
#f.write('Point({0}) = {{ {1} ,  {2} , 0., lc1 }} ;\n'.format(3+off_set_points,off_set,y_size))
#s_line_loop = ""
#for i in range(0, 3):
#    f.write('Line({0}) = {{ {1} ,  {2} }} ;\n'.format(i+off_set_lines,off_set_points+i,off_set_points+i+1))
#    s_line_loop = s_line_loop  +str(i+off_set_lines)+ ", "
#f.write('Line({0}) = {{ {1} ,  {2} }} ;\n'.format(3+off_set_lines,off_set_points+3,off_set_points+0))
#s_line_loop = s_line_loop  +str(4+off_set_lines-1)
#f.write('Line Loop({0}) = {{ {1} }} ;\n'.format(off_set_lines_loop,s_line_loop))  
#f.write('Plane Surface( {0} ) = {{{0}}};\n'.format(off_set_surface))
#if recombine == 1 :
#    f.write('Recombine Surface( {0} );\n'.format(off_set_surface)) 
#        
#off_set_points = off_set_points + 4
#off_set_lines  = off_set_lines +  4
#off_set_lines_loop = off_set_lines_loop + 1
#off_set_surface = off_set_surface +1


s_Plane_loop=""
for i in range(0, len(contours)-1):
    s_Plane_loop = s_Plane_loop + str(i+1)+ ", "
    
s_Plane_loop = s_Plane_loop + str(len(contours))
f.write('Plane Surface( {0} ) = {{{1}}};\n'.format(off_set_surface,s_Plane_loop))
if recombine==1 :
    f.write('Recombine Surface( {0} );\n'.format(off_set_surface))

    
f.write('Physical Surface("{0}") = {{{1}}};\n'.format("DOMAIN_GRAIN",s_Plane_loop[3:])) 
f.write('Physical Surface("{0}") = {{-{1}}};\n'.format("DOMAIN_JOIN",off_set_surface))    
off_set_surface=off_set_surface+1

f.write('Delete { \n')
f.write('  Surface{{{0}}};\n'.format(s_Plane_loop))
f.write('} \n')

#f.write('Delete { \n   Surface{1}; \n } \n')
f.write('Physical Line("LEFT")   = {1}; \n')
f.write('Physical Line("RIGHT")   = {3}; \n')
f.write('Physical Line("BOT")  = {4}; \n')
f.write('Physical Line("TOP") = {2}; \n')




f.close()
