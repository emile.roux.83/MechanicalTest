import MechanicalTest as mt
import os

#-------------------------------------------------------------------------------
# 2D Tensil test WITH MECHANICALTESR + ARGIOPE + ABAQUS
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# USEFUL FUNCTIONS
def create_dir(path):
  try:
    os.mkdir(path)
  except:
    pass
#-------------------------------------------------------------------------------
# MESH From IMG
execfile("Contout2MeshDev.py")

#-------------------------------------------------------------------------------
# SETTINGS
execfile("local_settings.py")
workdir   = "workdir/"
outputdir = "outputs/"
simName   = "Traction_2D_VER_Sim2"

#-------------------------------------------------------------------------------


create_dir(workdir)
create_dir(workdir + outputdir)

#-------------------------------------------------------------------------------
# MESH DEFINITIONS
sample_mesh = mt.models.sample_mesh_2D_VER(
                                   gmsh_path = GMSH_PATH,
                                   workdir = workdir,
                                   lc1 = 10)
                                   

sample_mesh.save(h5path = workdir + outputdir + simName + "_sample_mesh.h5")
   
#-------------------------------------------------------------------------------
# STEP DEFINTIONS
steps = [
        mt.models.tension_2D_step_input(name = "LOADING1",
                                            control_type = "disp", 
                                            duration = 1., 
                                            nframes = 10,
                                            controlled_value = 2,
                                            template_dir = "TensileTest2D_VER"),
        mt.models.tension_2D_step_input(name = "LOADING2",
                                            control_type = "disp", 
                                            duration = 1., 
                                            nframes = 20,
                                            controlled_value = -2,
                                            template_dir = "TensileTest2D_VER"),
#        mt.models.tension_2D_step_input(name = "LOADING3",
#                                            control_type = "disp", 
#                                            duration = 1., 
#                                            nframes = 20,
#                                            controlled_value = 5,
#                                            template_dir = "TensileTest2D_VER"),
#        mt.models.tension_2D_step_input(name = "LOADING4",
#                                            control_type = "disp", 
#                                            duration = 1., 
#                                            nframes = 20,
#                                            controlled_value = -5,
#                                            template_dir = "TensileTest2D_VER"),
#        mt.models.tension_2D_step_input(name = "LOADING5",
#                                            control_type = "disp", 
#                                            duration = 1., 
#                                            nframes = 20,
#                                            controlled_value = 5,
#                                            template_dir = "TensileTest2D_VER"),
        ]                                                                                                  
#-------------------------------------------------------------------------------
#   WORKFLOW DEFINITION                              
mt.models.tension_2D_input(sample_mesh   = sample_mesh,
                            steps = steps ,
                            path = workdir + simName + ".inp",
                            template_dir = "TensileTest2D_VER")
                                      
mt.postproc.Traction_abqpostproc(
        workdir     =  workdir, 
        simName     = simName,
        template_dir = "TensileTest2D_VER")
        
mt.postproc.Traction_pypostproc(
        workdir     =  workdir, 
        simName        = simName,
        template_dir = "TensileTest2D_VER") 
                                             
#-------------------------------------------------------------------------------
#   WORKFLOX EXECUTION
mt.models.run(simName=simName,
              ABAQUS_PATH=ABAQUS_PATH,
              deleteOldFiles = True,
              cpus=1,
              workdir=workdir)
mt.postproc.run_postprocs(workdir=workdir,
                          simName=simName,
                          ABAQUS_PATH=ABAQUS_PATH)